title: 'Installing p2pool for Vertcoin mining'
date: 2014-05-02 23:42:13
tags: p2pool vertcoin
---

p2pool for scrypt-n coins is actually a little different due to the nature of the algorithm differences.

# Minimum Requirements
* A server to host your p2pool instance, such as a server from [DigitalOcean](https://www.digitalocean.com/?refcode=28f34d44946a)
* Ubuntu 13.04 or greater

# Installation

## Installing p2pool

First we'll install the needed packages

	sudo apt-get install libpython2.7-dev python-zope.interface python-twisted python-twisted-web

To help secure your p2pool instance a bit more, we'll create a new user for p2pool and switch over to that user

	sudo adduser p2pool sudo
	sudo su - p2pool

Grab the latest p2pool source code

	git clone https://github.com/donSchoe/p2pool-n.git

Now we'll compile the vertcoin module

	cd py_modules/vertcoin_scrypt
	sudo python setup.py install

# Running p2pool

Using the rpcuser and rpcpasswd found in your vertcoin.conf, fill in the values in the diamond brackets

	./run_p2pool.py <rpcuser> <rpcpassword> --net <network>

You have these options for the Vertcoin mining network:

* vertcoin1 (hash rate 3mh/s or up)
* vertcoin2 (hash rate 1.5mh/s or up)
* vertcoin3 (Hash rate 1.5mh/s or below)

Once you execute the command, your p2pool should be up and running. You can confirm by going to your browser and going to `http://<server ip>:9147`.
