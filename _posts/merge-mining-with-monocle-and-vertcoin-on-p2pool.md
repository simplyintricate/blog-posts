title: 'Merge Mining with Monocle and Monocle on p2pool with Ubuntu 14.04'
date: 2014-05-03 16:14:18
tags:
---

The recently released [Monocle](https://monocle.monocle.org/) (MON) Altcoin offers a new opportunity to mine multiple coins at the same time with [Monocle](https://monocle.org/). 

The new merged mining method is available to people p2pool node operators who install additional software to allow for the coin to be merged mined. For miners, it's as simple as adding the Monocle address of your choice as your password!

# Minimum Requirements
* Ubuntu 13.04 or greater
* A server to host your Ubuntu, such as on [DigitalOcean](https://www.digitalocean.com/?refcode=28f34d44946a)
* Some basic command line interface knowledge

# Installing the Monocle Wallet

The first step is to grab the Monocle source or to download the binary from package.

As of today, the Linux package is unavailable for the latest release. Therefore, we will compile from source.

## Compiling Monocle from source

First we'll grab the latest source code from github

	git clone https://github.com/erkmos/monocle.git
	cd monocle

If you have previously compiled bitcoind or a similar altcoin before, you'll likely have the prerequisite packages. If not, you'll need to install these:

	sudo apt-get install build-essential libboost-dev libboost-system-dev libboost-filesystem-dev libboost-program-options-dev libboost-thread-dev libssl-dev libdb++-dev libminiupnpc-dev qt5-qmake libqt5gui5 libqt5core5 libqt5dbus5 qttools5-dev-tools

Now you can compile the source

	cd src
	make -f makefile.unix

Once this is done, you should have monocled in your current working directory!

To maintain consistency with the packaged version of monocled, go ahead and move the binary to `/usr/bin/`.

	sudo mv monocled /usr/bin/

## Setting up Monocled as a service

Once you've installed monocled, you'll most likely want to be able to close your terminal without killing monocled itself. The easy option would be to use `nohup monocled`, but it's better to run it as a service.

The bitcoin community has a great post on [how to set up the bitcoind instance as a service](https://bitcointalk.org/index.php?topic=25518.0).

To start, create a new file under `/etc/init/monocle.conf`

	description "monocled"

	start on filesystem
	stop on runlevel [!2345]
	oom never
	expect daemon
	respawn
	respawn limit 10 60 # 10 times in 60 seconds

	script
	user=monocle
	home=/home/$user
	cmd=/usr/bin/monocled
	pidfile=$home/monocled.pid
	# Don't change anything below here unless you know what you're doing
	[[ -e $pidfile && ! -d "/proc/$(cat $pidfile)" ]] && rm $pidfile
	[[ -e $pidfile && "$(cat /proc/$(cat $pidfile)/cmdline)" != $cmd* ]] && rm $pidfile
	exec start-stop-daemon --start -c $user --chdir $home --pidfile $pidfile --startas $cmd -b --nicelevel 15 -m
	end script

Now go ahead and instantiate the file

	sudo initctl reload-configuration

The next step is to create the user that will run monocle. This will help enhance the security of the monocled process by limiting all activity to its own user space.

	sudo adduser monocle

Now we will need to create the monocle configuration so that we can start the daemon with the rpc server enabled.
The first step in this is to generate a random server password. You can do that in any number of ways, but the easiest is to run the monocled process.

	monocled
	Error: To use monocled, you must set a rpcpassword in the configuration file:
	/home/monocle/.monocle/monocle.conf
	It is recommended you use the following random password:
	rpcuser=monoclerpc
	rpcpassword=GLGwQQ5RE3KYaBdZgQE4aXdtHapFFiCe2UN9Pnr6tTG9

Go ahead and copy the rpcuser and rpcpassword and paste it into /home/monocle/.monocle/monocle.conf

Now, you should be ready to start up monocled!

	sudo start monocled

You can also check on the status of the monocled instance

	 sudo tail -f ~monocle/.monocle/debug.log

And there you have it! Monocled is now running as a full node on your own Ubuntu server.

## Installing the Stratum Proxy software

The next step is to install the startum proxy software to be able to support merge mining.

	sudo apt-get install redis-server python-pip mysql-server haskell-platform libmysqlclient-dev python-mysqldb libssl-dev python-redis
	sudo pip install bunch python-jsonrpc

We'll create a new user for added security and create a logging directory for it

	sudo adduser proxypool
	sudo mkdir /var/log/proxypool
	sudo chown proxpool /var/log/proxypool
	sudo su - proxypool
	
Now grab the source code

	git clone https://github.com/erkmos/proxypool
	cd proxypool

For the proxy pool, you use cabal to install

	cabal update
	cabal install cabal-install

	cabal sandbox init
	cabal --force-reinstalls install "aeson >=0.6.2.1" "async >=2.0.1.5" "base16-bytestring >=0.1" "cryptohash >=0.11" "either >=4.1" "hedis >=0.6.3" "hslogger >=1.2.3" "network >=2.4.2.2" "select >=0.4.0.1" "text >=1.1.1.1" "unordered-containers >=0.2.3.3" "vector >=0.10.9.1"
	cabal configure
	cabal build

Now you'll have to configure the proxy.

	cp proxypool.json.example proxypool.json

Now you'll need to create the MySQL User to track payouts

	mysql -u root -p
	CREATE USER 'proxypool'@'localhost' IDENTIFIED BY 'insert-password-here';
	create database proxypool;
	grant all privileges on proxypool.* to proxypool@localhost identified by 'insert-password-here';
	flush privileges;
	quit

To configure the payout tracker, open up `payout/sharelogger.conf` in your `proxypool` folder

Inside your `payout` folder, import the sql structure

	mysql -u root -p proxypool < db.sql

We'll also create a service script to run the server as a daemon. Open your favorite text editor to `/etc/init.d/proxypool` and paste the following contents

	#!/bin/sh

	DIR=/home/proxypool/proxypool
	DAEMON=$DIR/dist/build/server/server
	DAEMON_NAME=proxypool
	DAEMON_USER=proxypool

	# The process ID of the script when it runs is stored here:
	PIDFILE=/var/run/$DAEMON_NAME.pid

	. /lib/lsb/init-functions

	do_start () {
	    log_daemon_msg "Starting system $DAEMON_NAME daemon"
	    start-stop-daemon --start --background --no-close --pidfile $PIDFILE --make-pidfile --user $DAEMON_USER --chuid $DAEMON_USER --chdir $DIR --startas $DAEMON > /var/log/proxypool/out.log 2>&1
	    log_end_msg $?
	}
	do_stop () {
	    log_daemon_msg "Stopping system $DAEMON_NAME daemon"
	    start-stop-daemon --stop --pidfile $PIDFILE --retry 10
	    log_end_msg $?
	}

	case "$1" in

	    start|stop)
		do_${1}
		;;

	    restart|reload|force-reload)
		do_stop
		do_start
		;;

	    status)
		status_of_proc "$DAEMON_NAME" "$DAEMON" && exit 0 || exit $?
		;;
	    *)
		echo "Usage: /etc/init.d/$DAEMON_NAME {start|stop|restart|status}"
		exit 1
		;;

	esac
	exit 0


Mark the script as executable and then run the server

	sudo chmod +x /etc/init.d/proxypool
	sudo /etc/init.d/proxypool start

You can check the log for debugging if needed

	tail -f /var/log/proxypool/out.log

It should say that it is authorized which means it is working.

The last thing to do is execute the payout script in the payout folder

	python payout.py

If all is good, you can run it in nohup which will survive your terminal being closed.

	nohup python payout.py &
