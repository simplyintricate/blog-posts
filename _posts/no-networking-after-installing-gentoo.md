title: 'No networking after installing Gentoo'
date: 2014-05-10 21:17:07
tags: Gentoo
---

After installing a hardened version of Gentoo on Virtualbox, I ran into an issue where the networking just didn't work. Pings to Google resulted in `Network Unreachable`. 

If you run `ifconfig -a`, I noticed that eth0 was not there but this weird enp0s3 was. Because of this, I thought that the issue was due to eth0 being missing (some driver issue, etc). Turns out though, the driver was already running and was working. The reason why eth0 doesn't exist anymore was because of an update in __udev__ which changed the interface names! Now, eth0 is enp0s3!

Anyway, if your networking is still not working in Virtualbox, it turns out that Gentoo somehow didn't create the `net.enp0s3` file, so we'll just copy the localhost one.

	cd /etc/init.d/
	ln -s net.lo net.enp0s3

Now restart the network

	service net.enp0s3 restart

With that, dhcp connected and I was able to ping google.
