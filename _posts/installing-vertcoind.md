title: Installing the Vertcoind Full Node on Ubuntu 14.04
date: 2014-05-02 21:49:33
tags: Vertcoin
---

I was recently interested in the new ASIC-resistant coin called [Vertcoin](https://vertcoin.org/). For mining purposes, you'll need a vertcoind instance to run p2pool. 

To start, you can grab any Virtual Private Server loaded with Ubuntu 13.04 or greater. 

# Minimum Requirements
* Ubuntu 13.04 or greater (This post details how to install using 14.04)
* A server to host Ubuntu, such as [DigitalOcean](https://www.digitalocean.com/?refcode=28f34d44946a)
* Some basic knowledge of how to use a Command Line Interface

# Downloading Vertcoind

The first step is to either compile Vertcoin from source or to install the packaged vertcoind instance. The latter is a bit easier but the former allows you greater flexibility in what kind of features you can get.

## Installing vertcoind from packages

To install Vertcoind from a packge, you'll need to add a PPA. If you would rather compile from source, skip this step.

	sudo add-apt-repository ppa:vertcoin/ppa
	sudo apt-get update
	sudo apt-get install vertcoind

You'll now have vertcoind installed and can skip the next step.

## Installing vertcoind from source

The first step is to obtain the vertcoin source code. You can obtain the Vertcoin source code from github

	git clone https://github.com/vertcoin/vertcoin.git

If you have previously compiled bitcoind or a similar altcoin before, you'll likely have the prerequisite packages. If not, you'll need to install these:

	sudo apt-get install build-essential libboost-dev libboost-system-dev libboost-filesystem-dev libboost-program-options-dev libboost-thread-dev libssl-dev libdb++-dev libminiupnpc-dev qt5-qmake libqt5gui5 libqt5core5 libqt5dbus5 qttools5-dev-tools
	
Once you've installed the prequisite packages, move to the src directory and compile the vertcoind binary

	cd src
	make -f makefile.unix

Once this is done, you should have vertcoind in your current working directory!

To maintain consistency with the packaged version of vertcoind, go ahead and move the binary to `/usr/bin/`.

	sudo mv vertcoind /usr/bin/

# Setting up Vertcoind as a service

Once you've installed vertcoind, you'll most likely want to be able to close your terminal without killing vertcoind itself. The easy option would be to use `nohup vertcoind`, but it's better to run it as a service.

The bitcoin community has a great post on [how to set up the bitcoind instance as a service](https://bitcointalk.org/index.php?topic=25518.0).

To start, create a new file under `/etc/init/vertcoin.conf`

	description "vertcoind"

	start on filesystem
	stop on runlevel [!2345]
	oom never
	expect daemon
	respawn
	respawn limit 10 60 # 10 times in 60 seconds

	script
	user=vertcoin
	home=/home/$user
	cmd=/usr/bin/vertcoind
	pidfile=$home/vertcoind.pid
	# Don't change anything below here unless you know what you're doing
	[[ -e $pidfile && ! -d "/proc/$(cat $pidfile)" ]] && rm $pidfile
	[[ -e $pidfile && "$(cat /proc/$(cat $pidfile)/cmdline)" != $cmd* ]] && rm $pidfile
	exec start-stop-daemon --start -c $user --chdir $home --pidfile $pidfile --startas $cmd -b --nicelevel 15 -m
	end script

Now go ahead and instantiate the file

	sudo initctl reload-configuration

The next step is to create the user that will run vertcoin. This will help enhance the security of the vertcoind process by limiting all activity to its own user space.

	sudo adduser vertcoin

Now we will need to create the vertcoin configuration so that we can start the daemon with the rpc server enabled.
The first step in this is to generate a random server password. You can do that in any number of ways, but the easiest is to run the vertcoind process.

	vertcoind
	Error: To use vertcoind, you must set a rpcpassword in the configuration file:
	/home/vertcoin/.vertcoin/vertcoin.conf
	It is recommended you use the following random password:
	rpcuser=vertcoinrpc
	rpcpassword=GLGwQQ5RE3KYaBdZgQE4aXdtHapFFiCe2UN9Pnr6tTG9

Go ahead and copy the rpcuser and rpcpassword and paste it into /home/vertcoin/.vertcoin/vertcoin.conf

Now, you should be ready to start up vertcoind!

	sudo start vertcoind

You can also check on the status of the vertcoind instance

	 sudo tail -f ~vertcoin/.vertcoin/debug.log

And there you have it! Vertcoind is now running as a full node on your own Ubuntu server.
